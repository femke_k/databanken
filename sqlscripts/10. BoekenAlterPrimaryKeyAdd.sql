-- FK
-- 18/04/2018
-- Kolom Id toevoegen en primary key van maken
--
use ModernWays;
alter Table Boeken add Column Id int auto_increment, 
        add constraint pk_Boeken_Id primary key (Id);