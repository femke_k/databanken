-- FK
-- 28 februari 2018
-- Werken met INSERT
-- Bestandsnaam: BoekenInsertAureliusAugustinus.sql
use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Category,
   InsertedBy
)
values (
   'Aurelius',
   'Augustinus',
   'De stad van God',
   'Baarn',
   'Uitgeverij Baarn',
   '1983',
   '1992',
   'Nog te lezen',
   'Theologie',
   'Femke Kennis'
);