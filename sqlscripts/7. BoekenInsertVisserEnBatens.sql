-- fk
-- 28/02/2018
-- twee rijen in een keer toevoegen
-- BoekenInsertVisserEnBatens.sql
use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values
('Aurelius', 'Augustinus', 'De stad van God', 'Baarn', 'Uitgeverij Baarn', '1983', '1992', '', '', ''),
('Diderik', 'Batens', 'Logicaboek', '', 'Garant', '1999', '', '', '', '');