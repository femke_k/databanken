-- FK
-- 14 maart 2018
-- Bestandsnaam: BoekenInsertAllJI.sql
-- gegevens komen uit de Korte Introductie
--
-- Opmerking
-- Als je een enkel aanhalingsteken in een string wil typen in SQL
-- moet je het enkel aanhalingstelen escapen omdat het enkel aanhalingsteken
-- eigenlijk een instructie is die aangeeft dat wat volgt een tekenreeks is.
-- Om een enkel aanhalingsteken te escapen ontdubbel je het bv:
-- 'Le siècle d''or'
-- Het eerste enkel aanhalingsteken begint de string, het tweede en derde geven
-- een enkel aanhalingsteken dat in de string wordt opgenomen en het vierde
-- sluit de string af.
-- Zet de use in commentaar om te voorkomen dat medecursisten gegevens
-- in jou tabel inserten. We gaan immers die insert scipts met elkaar delen.
use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values 
-- nog toe te voegen!!
