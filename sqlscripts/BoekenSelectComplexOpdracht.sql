-- FK
-- 29/03/2018
-- afstandsles Opdracht les 8

-- Oefening 1
use ModernWays;
select Familienaam, Titel, Verschijningsjaar
from Boeken
where not (Familienaam like 'D%' or Familienaam like 'K%' or Familienaam like 'R%')
and (Verschijningsjaar like '192_' or Verschijningsjaar like '199_' );

-- Oefening 2
use ModernWays;
select Familienaam, Titel, Verschijningsjaar
from Boeken
where(Familienaam like 'D%' or Familienaam like 'K%' or Familienaam like 'R%')
and (Verschijningsjaar like '19_2');