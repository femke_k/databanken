-- FK
-- 21 februari 2018
--
-- naam van scriptbestand is PostcodesCreate
-- volgt het patroon Tabelnaam + DDL statement

USE ModernWays;
DROP TABLE IF EXISTS 'Postcodes';
CREATE TABLE Postcodes(
	Code CHAR (4),
    Plaats NVARCHAR (50),
    Localite NVARCHAR (50),
    Provincie NVARCHAR (50),
    Province NVARCHAR (50)
    );